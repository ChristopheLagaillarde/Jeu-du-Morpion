/*/////////////////En t�te////////////////////////////
 * Programme : Jeu du Morpion.
 *
 * Description : Ce programme g�n�re la fen�tre du jeu
 *               du Morpion. 
 *
 * Auteur : Christophe LAGAILLARDE
 *
 * Date : 28/03/2021
 *        
 *///////////////////////////////////////////////////

// D�claration des bibliotheques de fonctions...
package jeu_du_morpion;
import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

//////////////////Le Programme principal/////////////

//////////////////D�but//////////////////////////////
class Joueur{

	// Attributs
	Boolean tourJoueur;                                 // Indique � quel joueur c'est le tours
	String oOuX;                                       // Indique si il faut mettre X ou O en fonction du tourJoueur
	String nomJoueur1;                                // Indique le nom du joueur1
	String nomJoueur2;                               // Indique le nom du joueur2
	String nomJoueurDuTour;                         // Indique le nom du joueur qui doit jouer
	String [][]enregistrement = new String[3][3];  // Matrice qui enregistre quel jouers a fait quel choix
	Color couleurDuJoueur;                         // Indique la couleur qu'auront les cases de la combinaison gagnante en fonction du joueur

	// Methodes

	// Fonction qui permet de sauvegarder le choix des joueurs
	String caseEnregistre(String oOuX) {  
		if(this.tourJoueur) {
			oOuX = "O";
		}
		if(!this.tourJoueur) {
			oOuX = "X";
		}
		return oOuX;
	}

	// Proc�dure qui permet de changer de tour
	static void changementDeJoueur(Joueur joueurQuiAJoue) {
		joueurQuiAJoue.tourJoueur = !joueurQuiAJoue.tourJoueur;
		if(joueurQuiAJoue.tourJoueur) {
			joueurQuiAJoue.oOuX = "O";
			joueurQuiAJoue.nomJoueurDuTour = joueurQuiAJoue.nomJoueur1;
			joueurQuiAJoue.couleurDuJoueur = Color.GREEN;
			
		}
		if(!joueurQuiAJoue.tourJoueur) {
			joueurQuiAJoue.oOuX = "X";
			joueurQuiAJoue.nomJoueurDuTour = joueurQuiAJoue.nomJoueur2;
			joueurQuiAJoue.couleurDuJoueur = Color.RED;

		}
	}	

	// Proc�dure qui v�rifie si il y a un gagnant
	static void quiAGagne(Joueur joueur1Ou2,Button button,Button button_1,Button button_2,Button button_3,Button button_4,Button button_5,Button button_6,Button button_7,Button button_8) { // Proc�dure qui v�rifie quel joueurs a gagn�
		int i = 0;
		int j = 0;
		String gagnant = "";                          // Variable qui permet de connaitre le gagnant    


		// On parcours les lignes de la matrice qui enregistre
		for(i = 0; i < 3; i++) {                  

			// On v�rifie si il y a un gagnant entre le parcours de chaque ligne
			if(gagnant.equals("XXX") || gagnant.equals("OOO")) {                   // si gagnant vaut 3 alors il y a trois cases jou� par joueur1 qui sont align�s
				JOptionPane.showMessageDialog(null,joueur1Ou2.nomJoueurDuTour + " a gagn�, bravo!");   // Permet d'afficher dans une fen�tre le nom du joueur qui a gagn�


				// Pour chaque lignes on colorie les cases qui ont fait gagn�

				if(i == 1) {
					button.setBackground(joueur1Ou2.couleurDuJoueur);                                      // Code qui change la couleurs d'un bouton 
					button_1.setBackground(joueur1Ou2.couleurDuJoueur);
					button_2.setBackground(joueur1Ou2.couleurDuJoueur);
				}
				if(i == 2) {
					button_3.setBackground(joueur1Ou2.couleurDuJoueur);                                         
					button_4.setBackground(joueur1Ou2.couleurDuJoueur);
					button_5.setBackground(joueur1Ou2.couleurDuJoueur);
				}

			}

			gagnant = "";                         // On r�initialise gagnant pour v�rifier d'autres lignes    


			// On parcours chaque �l�ments de la ligne (en changeant de colonne)
			for(j = 0; j < 3; j++) {           
				gagnant += joueur1Ou2.enregistrement[i][j]; // On calcule la somme des valeurs des �l�ments align�, en fonction de la valeurs qu'on aura on connaitra le gagnant
			}
		}

		// On v�rifie si il y a un gagnant pour la derni�re ligne
		if(gagnant.equals("XXX") || gagnant.equals("OOO")) {         
			JOptionPane.showMessageDialog(null,joueur1Ou2.nomJoueurDuTour + " a gagn�, bravo!");   

			if(i == 3) {
				button_6.setBackground(joueur1Ou2.couleurDuJoueur);                                         
				button_7.setBackground(joueur1Ou2.couleurDuJoueur);
				button_8.setBackground(joueur1Ou2.couleurDuJoueur);
			}

		}


		gagnant = "";                                 


		// On parcours les colonnes de la matrice qui enregistre
		for(j = 0; j < 3; j++) {                                 // Pour avoir des explications se r�f�rer au d�but de la proc�dure QuiAGagne


			if(gagnant.equals("XXX") || gagnant.equals("OOO")) {               
				JOptionPane.showMessageDialog(null,joueur1Ou2.nomJoueurDuTour + " a gagn�, bravo!"); 


				if(j == 1) {
					button.setBackground(joueur1Ou2.couleurDuJoueur);                                         
					button_3.setBackground(joueur1Ou2.couleurDuJoueur);
					button_6.setBackground(joueur1Ou2.couleurDuJoueur);
				}

				if(j == 2) {
					button_1.setBackground(joueur1Ou2.couleurDuJoueur);                                        
					button_4.setBackground(joueur1Ou2.couleurDuJoueur);
					button_7.setBackground(joueur1Ou2.couleurDuJoueur);
				}

			}


			gagnant = "";                                                  // Pour avoir des explications se r�f�rer au d�but de la proc�dure QuiAGagne


			// On parcours chaque �l�ments de la colonne (en changeant de ligne)
			for(i = 0; i < 3; i++) {                                        
				gagnant += joueur1Ou2.enregistrement[i][j];                         
			}
		}

		// On v�rifie si il y a un gagnant pour la derni�re colonne
		if(gagnant.equals("XXX") || gagnant.equals("OOO")) {                                                 
			JOptionPane.showMessageDialog(null,joueur1Ou2.nomJoueurDuTour + " a gagn�, bravo!");   
			if(j == 3) {
				button_2.setBackground(joueur1Ou2.couleurDuJoueur);                                         
				button_5.setBackground(joueur1Ou2.couleurDuJoueur);
				button_8.setBackground(joueur1Ou2.couleurDuJoueur);
			} 		

		}


		gagnant = "";    


		// On parcours la diagonal descendante
		for(i = 0; i < 3; i++) {                                    // Pour avoir des explications se r�f�rer au d�but de la proc�dure QuiAGagne
			j = i;

			gagnant += joueur1Ou2.enregistrement[i][j];                                             

			// On v�rifie si il y a un gagnant
			if(gagnant.equals("XXX") || gagnant.equals("OOO")) {              
				JOptionPane.showMessageDialog(null,joueur1Ou2.nomJoueurDuTour + " a gagn�, bravo!"); 
				button.setBackground(joueur1Ou2.couleurDuJoueur);                                        
				button_4.setBackground(joueur1Ou2.couleurDuJoueur);
				button_8.setBackground(joueur1Ou2.couleurDuJoueur);
			}

		}


		gagnant = "";      


		// On parcour la diagonal montante de la matrice
		j =3;                                                                   // Pour avoir des explications se r�f�rer au d�but de la proc�dure QuiAGagne
		for(i = 0; i < 3; i++) {
			j--;


			gagnant += joueur1Ou2.enregistrement[i][j];  


			// On v�rifie si il y a un gagnant
			if(gagnant.equals("XXX") || gagnant.equals("OOO")) {               
				JOptionPane.showMessageDialog(null,joueur1Ou2.nomJoueurDuTour + " a gagn�, bravo!"); 
				button_2.setBackground(joueur1Ou2.couleurDuJoueur);                                
				button_4.setBackground(joueur1Ou2.couleurDuJoueur);
				button_6.setBackground(joueur1Ou2.couleurDuJoueur);
			}

		}

	}
	
}

public class JeuMorpion {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JeuMorpion window = new JeuMorpion();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JeuMorpion() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		// Initialisation des variables
		final Joueur joueur1Ou2 = new Joueur();      // Permet de cr�e un objet joueur qui va repr�senter le joueur qui joue � un moment donn�
		joueur1Ou2.tourJoueur = true;                // Permet d'initialiser pour le joueur1 qui joue O
		joueur1Ou2.oOuX = "O";
		joueur1Ou2.nomJoueur1 = JOptionPane.showInputDialog(null, "Entrez le nom du Joueur1 :"); // Permet d'afficher deux fen�tres pour demander le nom des joueurs
		joueur1Ou2.nomJoueur2 = JOptionPane.showInputDialog(null, "Entrez le nom du Joueur2 :"); 
		joueur1Ou2.couleurDuJoueur = Color.RED;

		frame = new JFrame();
		frame.setBounds(100, 100, 238, 358);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		final JTextPane txtpnVfj = new JTextPane();
		txtpnVfj.setBounds(10, 257, 205, 20);
		frame.getContentPane().add(txtpnVfj);

		// Initialisation de tout les boutons ici
		final Button button = new Button("");     // Cela permet de les utiliser dans notre pro�dure quiAgagne apr�s
		final Button button_1 = new Button(""); 
		final Button button_2 = new Button(""); 
		final Button button_3 = new Button(""); 
		final Button button_4 = new Button(""); 
		final Button button_5 = new Button(""); 
		final Button button_6 = new Button(""); 
		final Button button_7 = new Button(""); 
		final Button button_8 = new Button(""); 
		JButton btnNewButton = new JButton("Recommencer");
		
		
		// Les commentaires de Button button (Bouton 1) valent pour les autres Button aussi
		
		// Le Bouton 1
		button.setBounds(10, 10, 70, 77);                 // Permet de placer la case dans la fen�tre et de lui d�finir une taille
		button.addActionListener(new ActionListener() {   // Permet de v�rifier les interaction avec la case
			public void actionPerformed(ActionEvent e) {  // Permet d'indiquer les actions � faire en cas de clique par la souris
				button.setLabel(joueur1Ou2.oOuX);         // Permet de mettre X ou O sur la case, en fonction de la valeur du String oOuX
				joueur1Ou2.enregistrement[0][0]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX); // Permet d'enregistrer le choix du joueur grace � l'indice i et j, et d'enregistrer quel joueur l'a fait gr�ce � la valeur de l'�l�ment 
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);  // Permet d'afficher une fen�tre qui indique le gagnant lorsqu'il y en a un
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour); // Permet d'afficher qui doit jouer apres le clique
			}
		});
		frame.getContentPane().add(button);

		
		
		// Le Bouton 2
		button_1.setBounds(77, 10, 70, 77);                   		// Voir commentaire du  1er Button en haut
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_1.setLabel(joueur1Ou2.oOuX);
				joueur1Ou2.enregistrement[0][1]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX);
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour);
			}
		});
		frame.getContentPane().add(button_1);               

		
		
		// Le Bouton 3
		button_2.setBounds(145, 10, 70, 77);                             		// Voir commentaire du  1er Button en haut
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_2.setLabel(joueur1Ou2.oOuX);
				joueur1Ou2.enregistrement[0][2]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX);
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour);
			}
		});
		frame.getContentPane().add(button_2);

		
		
		// Le Bouton 4
		button_3.setBounds(10, 91, 70, 77);                               		// Voir commentaire du  1er Button en haut
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_3.setLabel(joueur1Ou2.oOuX);
				joueur1Ou2.enregistrement[1][0]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX);
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour);
			}
		});
		frame.getContentPane().add(button_3);          

		
		
		// Le Bouton 5
		button_4.setBounds(77, 91, 70, 77);                                		// Voir commentaire du  1er Button en haut
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_4.setLabel(joueur1Ou2.oOuX);
				joueur1Ou2.enregistrement[1][1]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX);
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour);
			}
		});
		frame.getContentPane().add(button_4);

		
		
		// Le Bouton 6
		button_5.setBounds(145, 91, 70, 77);                               		// Voir commentaire du  1er Button en haut
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_5.setLabel(joueur1Ou2.oOuX);
				joueur1Ou2.enregistrement[1][2]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX);
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour);
			}
		});
		frame.getContentPane().add(button_5);

		
		
		// Le Bouton 7
		button_6.setBounds(10, 174, 70, 77);                                   		// Voir commentaire du  1er Button en haut
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_6.setLabel(joueur1Ou2.oOuX);
				joueur1Ou2.enregistrement[2][0]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX);
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour);
			}
		});
		frame.getContentPane().add(button_6);

		
		
		// Le Bouton 8
		button_7.setBounds(77, 174, 70, 77);                                        		// Voir commentaire du  1er Button en haut
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				button_7.setLabel(joueur1Ou2.oOuX);
				joueur1Ou2.enregistrement[2][1]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX);
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour);
			}
		});
		frame.getContentPane().add(button_7);

		
		
		// Le Bouton 9
		button_8.addActionListener(new ActionListener() {                                   		// Voir commentaire du  1er Button en haut
			public void actionPerformed(ActionEvent e) {
				button_8.setLabel(joueur1Ou2.oOuX);
				joueur1Ou2.enregistrement[2][2]= joueur1Ou2.caseEnregistre(joueur1Ou2.oOuX);
				Joueur.quiAGagne(joueur1Ou2, button, button_1, button_2, button_3, button_4, button_5, button_6, button_7,button_8);
				Joueur.changementDeJoueur(joueur1Ou2);
				txtpnVfj.setText("C'est le tour de "+joueur1Ou2.nomJoueurDuTour);
			}
		});
		button_8.setBounds(145, 174, 70, 77);
		frame.getContentPane().add(button_8);

		
		
		// Le Bouton Recommancer
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i;
				int j;
				button.setLabel("");       // On efface tout les X et O sur les cases
				button_1.setLabel("");
				button_2.setLabel("");
				button_3.setLabel("");
				button_4.setLabel("");
				button_5.setLabel("");
				button_6.setLabel("");
				button_7.setLabel("");
				button_8.setLabel("");
				button.setBackground(null);                 // Les couleurs des cases redevienne grises
				button_1.setBackground(null);
				button_2.setBackground(null);
				button_3.setBackground(null);
				button_4.setBackground(null);
				button_5.setBackground(null);
				button_6.setBackground(null);
				button_7.setBackground(null);
				button_8.setBackground(null);

				joueur1Ou2.tourJoueur = true;              // On remet toute les variables � leurs valeurs initial
				joueur1Ou2.oOuX = "O";
				joueur1Ou2.couleurDuJoueur = Color.RED;
				for(i = 0; i < 3; i++) {
					for(j = 0; j < 3; j++) {
						joueur1Ou2.enregistrement[i][j] = "";
					}
				}
				joueur1Ou2.nomJoueur1 = JOptionPane.showInputDialog(null, "Entrez le nom du Joueur1 :"); // On redemande les noms pour une nouvelle partie
				joueur1Ou2.nomJoueur2 = JOptionPane.showInputDialog(null, "Entrez le nom du Joueur2 :");
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnNewButton.setBounds(46, 285, 133, 23);
		frame.getContentPane().add(btnNewButton);
	}
}
//////////////////Fin//////////////////////////////